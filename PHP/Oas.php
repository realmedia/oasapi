<?php namespace Models;

/**
*
*/
class Oas {

  const Advertiser = 'Advertiser';
  const AdvertiserCategory = 'AdvertiserCategory';
  const Affiliate = 'Affiliate';
  const Agency = 'Agency';
  const CampaignGroup = 'CampaignGroup';
  const CompanionPosition = 'CompanionPosition';
  const CompetitiveCategory = 'CompetitiveCategory';
  const ConversionProcess = 'ConversionProcess';
  const CreativeType = 'CreativeType';
  const Zone = 'Zone';
  const Event = 'Event';
  const Page = 'Page';
  const InsertionOrder = 'InsertionOrder';
  const RichMediaTemplate = 'RichMediaTemplate';
  const SalesPerson = 'SalesPerson';
  const Section = 'Section';
  const Site = 'Site';
  const SiteGroup = 'SiteGroup';
  const Transaction = 'Transaction';
  const Keyword = 'Keyword';
  const Keyname = 'Keyname';
  const Product = 'Product';
  const Publisher = 'Publisher';
  const Campaign = 'Campaign';
  const CreativeTarget = 'CreativeTarget';
  const Creative = 'Creative';
  const Notification = 'Notification';

  private $noDatabaseNode = array('InsertionOrder', 'Campaign', 'CreativeTarget', 'Creative', 'Notification');
  private $codes = array('Bandwidth', 'Browser', 'BrowserV', 'Continent', 'Country', 'DMA', 'EventType', 'HourOfDay', 'Msa', 'Omniture', 'Os', 'Position', 'State', 'TopDomain', 'WeekDay', 'Zone', 'City', 'Carrier', 'DeviceGroup', 'Device', 'Manufacturer');

  private $account;
  private $username;
  private $password;
  private $endpoint;

  public function __construct($username, $password, $account, $endpoint = 'https://openadstream17.247realmedia.com/oasapi/OaxApi?wsdl'){
    $this->account = $account;
    $this->username = $username;
    $this->password = $password;
    $this->endpoint = $endpoint;
  }

  private function load($query = null){
    $client = new \SoapClient($this->endpoint, array('trace' => true, 'exceptions' => true));
    $request = $client->OasXmlRequest($this->account, $this->username, $this->password, $query->asXml());

    if (is_soap_fault($request)) {
      throw new \Exception("SOAP Fallo: (faultcode: {$request->faultcode}, faultstring: {$request->faultstring})");
    } else {
      $xml = new \SimpleXMLElement($request);
      $response = $xml->xpath('/AdXML/Response')[0];

      if($response->Exception) throw new \Exception($response->Exception);

      return $response;
    }

    return false;
  }

  private function isCode($entity){
    return in_array($entity, $this->codes);
  }

  private function createXmlRequest($entity, $args, $action){
    if($this->isCode($entity) && $action !== 'list'){
      throw new \Exception("Code $entity can only be listed");
    } elseif (in_array($entity, [self::CompanionPosition, self::CompetitiveCategory]) && !in_array($action, ['list', 'add'])) {
      throw new \Exception("$entity can only be listed or added");
    } elseif (in_array($entity, [self::Campaign, self::Creative, self::CreativeTarget]) && $action === 'copy') {
      throw new \Exception("$entity can not be copied");
    }

    $xml = new \SimpleXMLElement("<AdXML><Request type='$entity'></Request></AdXML>");
    $request = $xml->Request;

    if(in_array($entity, $this->noDatabaseNode)){
      $child = $request->addChild($entity);
      $child->addAttribute('action', $action);

      if($action === 'list'){
        $criteria = $child->addChild('SearchCriteria');
        $this->arrayToXml($args, $criteria);
      } else {
        $this->arrayToXml($args, $child);
      }
    } else {
      $database = $request->addChild('Database');
      $database->addAttribute('action', $action);

      if($action === 'list'){
        $child = $database->addChild('SearchCriteria');
      } else {
        if($entity === 'Page' && isset($args['Url']) && is_numeric(key($args['Url']))){
          $child = $database->addChild('Pages');
        } else {
          $child = $database->addChild($entity);
        }
      }

      $this->arrayToXml($args, $child);
    }

    return $xml;
  }

  /*************** Helpers **********************************************/
  private function arrayToXml($array, &$xml){
    foreach($array as $key => $value) {
      if (strpos($key, "@") === 0){
        $xml->addAttribute(str_replace("@", "", $key), $value);
      } elseif(is_array($value)) {
        if(is_numeric(key($value))) {
          foreach($value as $k=>$v){
            $xml->addChild($key, $v);
          }
        } else {
          $subnode = $xml->addChild($key);
          $this->arrayToXml($value, $subnode);
        }
      } else {
        $xml->addChild($key, $value);
      }
    }
  }
  /*************** Fim Helpers ******************************************/

  public function add($entity, $args = []){
    if($entity === self::SalesPerson){
      $entity = 'Salesperson';
      // TODO: verificar se é isso mesmo ou se é erro de documentação
    }

    $xml = $this->createXmlRequest($entity, $args, 'add');
    $result = $this->load($xml);

    return $result->xpath("/$entity");
  }

  public function read($entity, $args = []){
    $xml = $this->createXmlRequest($entity, $args, 'read');
    $result = $this->load($xml);

    return $result->xpath("/$entity");
  }

  public function _list($entity, $args = [], $pagination = []){
    $xml = $this->createXmlRequest($entity, $args, 'list');

    if(!empty($pagination) && $entity != self::Notification && !$this->isCode($entity)){
      $criteria = $result->xpath("SearchCriteria");
      foreach ($pagination as $key => $value) {
        $criteria->addAttribute($key, $value);
      }
    }

    $result = $this->load($xml);

    return $result->xpath("List/$entity");
  }

  public function update($entity, $args = []){
    $xml = $this->createXmlRequest($entity, $args, 'update');
    $result = $this->load($xml);

    return $result->xpath("/$entity");
  }

  public function _copy($entity, $args = []){
    $xml = $this->createXmlRequest($entity, $args, 'copy');
    $result = $this->load($xml);

    return $result->xpath("/$entity");
  }

  public function testLive(){
    $args = new \SimpleXMLElement("<TestLiveCampaigns/>");
    $xml = $this->createXmlRequest('Campaign', $args, 'run');

    $result = $this->load($xml);

    return $result->xpath('/Campaign/TestLiveCampaigns');
  }

  public function runLive(){
    $args = new \SimpleXMLElement("<RunLiveCampaigns/>");
    $xml = $this->createXmlRequest('Campaign', $args, 'run');

    $result = $this->load($xml);

    return $result->xpath('/Campaign/RunLiveCampaigns');
  }

  public function lastRLC(){
    $args = new \SimpleXMLElement("<LastStatusRLC/>");
    $xml = $this->createXmlRequest('Campaign', $args, 'run');

    $result = $this->load($xml);

    return $result->xpath('/Campaign/LastStatusRLC');
  }

}
