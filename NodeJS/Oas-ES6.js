import fs from 'fs';
import soap from 'soap';
import xml2js from 'xml2js';
import xmlbuilder from 'xmlbuilder';

class Oas {

  constructor(username, password, account, endpoint = 'https://openadstream17.247realmedia.com/oasapi/OaxApi?wsdl'){
    this.account = account;
    this.username = username;
    this.password = password;
    this.endpoint = endpoint;

    this.noDatabaseNode = ['InsertionOrder', 'Campaign', 'CreativeTarget', 'Creative', 'Notification'];
    this.codes = ['Bandwidth', 'Browser', 'BrowserV', 'Continent', 'Country', 'DMA', 'EventType', 'HourOfDay', 'Msa', 'Omniture', 'Os', 'Position', 'State', 'TopDomain', 'WeekDay', 'Zone', 'City', 'Carrier', 'DeviceGroup', 'Device', 'Manufacturer'];
  }

  load(query){
    let args = {account: this.account, username: this.username, password: this.password, xml: xmlbuilder.create(query)};

    return new Promise(function(resolve, reject){
      soap.createClient(this.endpoint, function(err, client) {
        client.OasXmlRequest(args, function(err, response) {
          xml2js.parseString(response.result, function (err, result) {
            let r = result.AdXML;
            if(r.Response[0].Exception){
              reject(r.Response[0].Exception[0]._);
            } else {
              resolve(r.Response);
            }
          });
        });
      });
    });
  }

  in_array(needle, haystack, argStrict){
    var key = '', strict = !!argStrict;

    if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;
        }
      }
    } else {
      for (key in haystack) {
        if (haystack[key] == needle) {
          return true;
        }
      }
    }

    return false;
  }

  isCode(entity){
    return this.in_array(entity, this.codes);
  }

  createRequestObject(entity, args, action){
    if(this.isCode(entity) && action !== 'list'){
      throw new Error(`Code ${entity} can only be listed`);
    } else if (this.in_array(entity, ["CompanionPosition", "CompetitiveCategory"]) && !this.in_array(action, ['list', 'add'])) {
      throw new Error(`${entity} can only be listed or added`);
    } else if (this.in_array(entity, ["Campaign", "Creative", "CreativeTarget"]) && action === 'copy') {
      throw new Error(`${entity} can not be copied`);
    }

    let xml = {'AdXML': {
      'Request': {
        '@type': entity
      }
    }};
    let request = xml.AdXML.Request;

    if(this.in_array(entity, this.noDatabaseNode)){
      request[entity] = {'@action': action};
      let child = request[entity];

      if(action === 'list'){
        child.SearchCriteria = args;
        let criteria = child.SearchCriteria;
      }
    } else {
      request.Database = {'@action': action};
      let database = request.Database;

      if(action === 'list'){
        database.SearchCriteria = {};
        let child = database.SearchCriteria;
      } else {
        if(entity === 'Page' && args.Url && Array.isArray(args.Url)){
          database.Pages = {};
          child = database.Pages;
        } else {
          database[entity] = {};
          child = database[entity];
        }
      }

      Object.assign(child, args);
    }

    return xml;
  }

  add(entity, args = {}){
    if(entity === "SalesPerson"){
      entity = 'Salesperson';
      // TODO: verificar se é isso mesmo ou se é erro de documentação
    }

    let xml = this.createRequestObject(entity, args, 'add');

    return this.load(xml);
  }

  read(entity, args = {}){
    let xml = this.createRequestObject(entity, args, 'read');

    return this.load(xml);
  }

  list(entity, args = {}, pagination = {}){
    let xml = this.createRequestObject(entity, args, 'list');

    if(!empty(pagination) && entity != "Notification" && !this.isCode(entity)){
      let criteria = xml.AdXML.SearchCriteria;
      pagination.forEach(function(value, key){
        criteria[`@${key}`] = value;
      });
    }

    return this.load(xml);
  }

  update(entity, args = {}){
    let xml = this.createRequestObject(entity, args, 'update');

    return this.load(xml);
  }

  copy(entity, args = {}){
    let xml = this.createRequestObject(entity, args, 'copy');

    return this.load(xml);
  }

  testLive(){
    let args = {TestLiveCampaigns: {}};
    let xml = this.createRequestObject('Campaign', args, 'run');

    return this.load(xml);
  }

  runLive(){
    let args = {RunLiveCampaigns: {}};
    let xml = this.createRequestObject('Campaign', args, 'run');

    return this.load(xml);
  }

  lastRLC(){
    let args = {LastStatusRLC: {}};
    let xml = this.createRequestObject('Campaign', args, 'run');

    return this.load(xml);
  }

}

export default Oas;
