'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _soap = require('soap');

var _soap2 = _interopRequireDefault(_soap);

var _xml2js = require('xml2js');

var _xml2js2 = _interopRequireDefault(_xml2js);

var _xmlbuilder = require('xmlbuilder');

var _xmlbuilder2 = _interopRequireDefault(_xmlbuilder);

var Oas = (function () {
  function Oas(username, password, account) {
    var endpoint = arguments.length <= 3 || arguments[3] === undefined ? 'https://openadstream17.247realmedia.com/oasapi/OaxApi?wsdl' : arguments[3];

    _classCallCheck(this, Oas);

    this.account = account;
    this.username = username;
    this.password = password;
    this.endpoint = endpoint;

    this.noDatabaseNode = ['InsertionOrder', 'Campaign', 'CreativeTarget', 'Creative', 'Notification'];
    this.codes = ['Bandwidth', 'Browser', 'BrowserV', 'Continent', 'Country', 'DMA', 'EventType', 'HourOfDay', 'Msa', 'Omniture', 'Os', 'Position', 'State', 'TopDomain', 'WeekDay', 'Zone', 'City', 'Carrier', 'DeviceGroup', 'Device', 'Manufacturer'];
  }

  _createClass(Oas, [{
    key: 'load',
    value: function load(query) {
      var args = { account: this.account, username: this.username, password: this.password, xml: _xmlbuilder2['default'].create(query) };

      return new Promise(function (resolve, reject) {
        _soap2['default'].createClient(this.endpoint, function (err, client) {
          client.OasXmlRequest(args, function (err, response) {
            _xml2js2['default'].parseString(response.result, function (err, result) {
              var r = result.AdXML;
              if (r.Response[0].Exception) {
                reject(r.Response[0].Exception[0]._);
              } else {
                resolve(r.Response);
              }
            });
          });
        });
      });
    }
  }, {
    key: 'in_array',
    value: function in_array(needle, haystack, argStrict) {
      var key = '',
          strict = !!argStrict;

      if (strict) {
        for (key in haystack) {
          if (haystack[key] === needle) {
            return true;
          }
        }
      } else {
        for (key in haystack) {
          if (haystack[key] == needle) {
            return true;
          }
        }
      }

      return false;
    }
  }, {
    key: 'isCode',
    value: function isCode(entity) {
      return this.in_array(entity, this.codes);
    }
  }, {
    key: 'createRequestObject',
    value: function createRequestObject(entity, args, action) {
      if (this.isCode(entity) && action !== 'list') {
        throw new Error('Code ' + entity + ' can only be listed');
      } else if (this.in_array(entity, ["CompanionPosition", "CompetitiveCategory"]) && !this.in_array(action, ['list', 'add'])) {
        throw new Error(entity + ' can only be listed or added');
      } else if (this.in_array(entity, ["Campaign", "Creative", "CreativeTarget"]) && action === 'copy') {
        throw new Error(entity + ' can not be copied');
      }

      var xml = { 'AdXML': {
          'Request': {
            '@type': entity
          }
        }
      };
      var request = xml.AdXML.Request;

      if (this.in_array(entity, this.noDatabaseNode)) {
        request[entity] = { '@action': action };
        var _child = request[entity];

        if (action === 'list') {
          _child.SearchCriteria = args;
          var criteria = _child.SearchCriteria;
        }
      } else {
        request.Database = { '@action': action };
        var database = request.Database;

        if (action === 'list') {
          database.SearchCriteria = {};
          var _child2 = database.SearchCriteria;
        } else {
          if (entity === 'Page' && args.Url && Array.isArray(args.Url)) {
            database.Pages = {};
            child = database.Pages;
          } else {
            database[entity] = {};
            child = database[entity];
          }
        }

        Object.assign(child, args);
      }

      return xml;
    }
  }, {
    key: 'add',
    value: function add(entity) {
      var args = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      if (entity === "SalesPerson") {
        entity = 'Salesperson';
        // TODO: verificar se é isso mesmo ou se é erro de documentação
      }

      var xml = this.createRequestObject(entity, args, 'add');

      return this.load(xml);
    }
  }, {
    key: 'read',
    value: function read(entity) {
      var args = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      var xml = this.createRequestObject(entity, args, 'read');

      return this.load(xml);
    }
  }, {
    key: 'list',
    value: function list(entity) {
      var args = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      var pagination = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

      var xml = this.createRequestObject(entity, args, 'list');

      if (!empty(pagination) && entity != "Notification" && !this.isCode(entity)) {
        (function () {
          var criteria = xml.AdXML.SearchCriteria;
          pagination.forEach(function (value, key) {
            criteria['@' + key] = value;
          });
        })();
      }

      return this.load(xml);
    }
  }, {
    key: 'update',
    value: function update(entity) {
      var args = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      var xml = this.createRequestObject(entity, args, 'update');

      return this.load(xml);
    }
  }, {
    key: 'copy',
    value: function copy(entity) {
      var args = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

      var xml = this.createRequestObject(entity, args, 'copy');

      return this.load(xml);
    }
  }, {
    key: 'testLive',
    value: function testLive() {
      var args = { TestLiveCampaigns: {} };
      var xml = this.createRequestObject('Campaign', args, 'run');

      return this.load(xml);
    }
  }, {
    key: 'runLive',
    value: function runLive() {
      var args = { RunLiveCampaigns: {} };
      var xml = this.createRequestObject('Campaign', args, 'run');

      return this.load(xml);
    }
  }, {
    key: 'lastRLC',
    value: function lastRLC() {
      var args = { LastStatusRLC: {} };
      var xml = this.createRequestObject('Campaign', args, 'run');

      return this.load(xml);
    }
  }]);

  return Oas;
})();

exports['default'] = Oas;
module.exports = exports['default'];
